package termek_gyakorlas.control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import termek_gyakorlas.model.Termek;

public class FileControl {

	File file;
	Scanner sc;
	
	public FileControl(String path) {
		super();
		this.file = new File(path);
		this.sc = new Scanner(System.in);
	}
	
	public void FileWriter(Termek termek,String kosar) {
		
		try {
			
			PrintWriter pw = new PrintWriter(this.file);
			
			pw.write(kosar + termek.toString());
			pw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public String FileReader() {
		
		StringBuilder sb = new StringBuilder();
		
		try (BufferedReader bf = new BufferedReader(new FileReader(this.file))) {
			
			String sor;
			
				while((sor = bf.readLine()) != null ) {
				System.out.println(sor);
				}
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	
}
